package com.devcamp.cmenuapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.cmenuapi.model.CMenu;
import com.devcamp.cmenuapi.repository.MenuRepository;

@Service
public class MenuService {
    @Autowired
    MenuRepository menuRepository;

    public List<CMenu> listMenus() {
        return menuRepository.findAll();
    }

}

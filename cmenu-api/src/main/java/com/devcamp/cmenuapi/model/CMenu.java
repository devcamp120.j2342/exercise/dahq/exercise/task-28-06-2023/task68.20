package com.devcamp.cmenuapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menus")
public class CMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String size;
    @Column(name = "duong_kinh")
    private String duongKinh;
    private String salad;
    @Column(name = "suon_nuong")
    private int suonNuong;
    @Column(name = "nuoc_ngot")
    private int nuocNgot;
    @Column(name = "thanh_tien")
    private long thanhTien;

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }

    public String getSalad() {
        return salad;
    }

    public void setSalad(String salad) {
        this.salad = salad;
    }

    public int getSuonNuong() {
        return suonNuong;
    }

    public void setSuonNuong(int suonNuong) {
        this.suonNuong = suonNuong;
    }

    public int getNuocNgot() {
        return nuocNgot;
    }

    public void setNuocNgot(int nuocNgot) {
        this.nuocNgot = nuocNgot;
    }

    public long getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(long thanhTien) {
        this.thanhTien = thanhTien;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
